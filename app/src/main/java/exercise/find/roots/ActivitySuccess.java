package exercise.find.roots;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.Nullable;

public class ActivitySuccess extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        Intent intentCreatedMe = getIntent();
        boolean check1 =intentCreatedMe.hasExtra("root1");
        boolean check2 =intentCreatedMe.hasExtra("root2");
        boolean check3 =intentCreatedMe.hasExtra("original_number");
        boolean check4 =intentCreatedMe.hasExtra("time");

        if (check1 && check2 && check3 && check4){
            long originalNumber = intentCreatedMe.getLongExtra("original_number", 0);
            long firstRoot = intentCreatedMe.getLongExtra("root1", 0);
            long secondRoot = intentCreatedMe.getLongExtra("root2", 0);
            double time = intentCreatedMe.getDoubleExtra("time", 0);



            TextView textView = findViewById(R.id.textViewSuccess);
            textView.setText(originalNumber + "=" + firstRoot + "*" + secondRoot + "   " + "time: " + time);

        }

    }
}
