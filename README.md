I pledge the highest level of ethical principles in support of academic excellence.  I ensure that all of my work reflects my own abilities and not those of someone else.


What would you change in the code in order to let the service run for maximum 200ms in tests
environments, but continue to run for 20sec max in the real app (production environment)?


in tests environments:
in test:
add this line :
intent.putExtra("abort", 0.2)
in service class:
add this line :
intent.getLongExtra("abort", 0.2)

use what we save in "abort" - 200ms = 0.2sec

in the real app:
use 20sec by default